---
layout: handbook-page-toc
title: "Critical System Tiering Methodology"
description: "The purpose of the Critical Systems Tiering Methodology is to support GitLab in identifying and understanding the specific systems utilized across the organization that are considered essential to order to maintain operations."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<!--HTML Parser Markup-->
{::options parse_block_html="true" /}

# Critical Systems Tiering Methodolgy

## Purpose

The purpose of the Critical Systems Tiering Methodology is to support GitLab in identifying and understanding the specific systems utilized across the organization that are considered essential to order to maintain operations. Ultimately, this provides GitLab with a mechanism to take a proactive approach to comprehensive risk management which considers all risks, such as information security and priavacy risks, impacting business operations across the organization. Additionally, by classifying systems into specific tiers, GitLab will be in a better position to appropriately prioritze risk mitigation activities and tailor internal controls based on a system's related tier.

## Scope

The critical systems tiering methodology is applicable to all systems utilized across GitLab which are tracked within the [Tech Stack](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) in order to ensure that all systems are vetted completely and accurately using a consistent and standardized methodology.

GitLab has taken an iterative approach to rolling out this methodology across the tech stack, beginning with systems that have been identified as being in-scope for external compliance audits. Over the course of FY22, this methodology will be applied to all systems within the tech stack. Once this has been accomplished, an annual review will be performed to to ascertain that each system's assigned tier is still accurate. This work is being performed in conjunction with the formal [Business Impact Analysis (BIA)](/handbook/business-technology/gitlab-business-continuity-plan/#business-impact-analysis) being executed in FY22. 

## Roles and Responisbilities

|Role|Responsibility|
|----------|------------------------------|
|[Security Risk Team](/handbook/engineering/security/security-assurance/risk-field-security/)|Executes an annual review of critical system tiers and makes adjustments as necessary. Owns the Critical System Tiering Methodolgy and supports the identification of and assignment of a critical system tier as needed.|
|[IT Compliance](/handbook/business-technology/it-compliance/)|Supports defining of critical system tier in conjunction with the Security Risk Team when new systems are added to the tech stack.|
|Technical System Owners|Provide complete and accurate data about the systems that they own so that an accurate tier is assigned.|

## Critical System Tiering Procedure

Defining what a critical system means at GitLab can be complex given the nature of our environment and the amount of integrations that exist across the many systems that are used to carry out business activities. In order to remain objective, the inputs used to determine system criticality tiers are the following:

1. If the system experienced a disruption or outage for less than 24 hours, would there be an impact to critical business processes?
2. Does the system integrate with other systems OR is the system utilized to facilitate/execute integrations (i.e. like a job scheduling and execution tool)?

Once the information is obtained, it is reviewed by the Security Risk and/or IT Compliance Team to determine which system tier should be assigned to the a system. The guidelines used to assign a tier are described in the [Determining Critical System Tiers](#determining-critical-system-tiers) section below.

**Note:** For systems that are onboarded to GitLab's environment prior to the creation of this methodology, inputs are identified through GitLab's [BIA activities](/handbook/business-technology/gitlab-business-continuity-plan/#business-impact-analysis), where technical system owners provide insight about the systems in which they own. The [Security Risk Team](/handbook/engineering/security/security-assurance/risk-field-security/) is working to incorporate these inputs as part of the security review performed during the procurement cycle. Until this process is in place, these inputs may be requested when new systems are added to the [Tech Stack](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml). In order to preserve the data residing within the tech stack, updated system tiering information will be provide via an interim document that is available for team members [here](https://docs.google.com/spreadsheets/d/1v6E48v2PujZJArSyZq1f3xvJ7wmwkUBZ8xSuKkBecfA/edit#gid=2108888748).
{: .note}

### Determining Critical System Tiers

Systems are assigned a critical system tier based on the following matrix:

| **Critical System Tier**<span style="color:#DB3B21;"><b>*</b></span> | If the system experienced a disruption or outage for less than 24 hours, would there be an impact to critical business processes? | _AND_ | Does the system integrate with other systems OR is the system utilized to facilitate/execute integrations (i.e. like a job scheduling and execution tool) |
|:-----:|:-----:|:-----:|:-----:|
|**Tier 1**<span style="color:#DB3B21;"><b>**</b></span>|YES|AND|YES|
|**Tier 2 Core**|YES|AND|NO|
|**Tier 2 Support**|NO|AND|YES|
|**Tier 3 / Non-critical**|NO|AND|NO|

<div class="panel panel-gitlab-purple">
**Notes**
{: .panel-heading}
<div class="panel-body">

<span style="color:#DB3B21;"><b>\*</b></span> As an extension of tiering methodology, the [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html) prescribes **specific** [Security and Privacy](/handbook/engineering/security/data-classification-standard.html#security-and-privacy-controls) control requirements for each data classification level. These requirements should be followed based on a system's data classification, regardless of the system's tier.
{: .note}

<span style="color:#DB3B21;"><b>\**</b></span> By default, any system that contains <b>RED Data</b> per the [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html#red) will be a Tier 1 system. This is explicitly due to the fact that this type of data is customer owned and uploaded data and as such, has been deemed to be critical in nature.
{: .note}

</div>
</div>

### Why does GitLab need this methodology?

Tiering systems utilized across GitLab enables team members to make decisions on prioritizing work related to a specific system(s) based on the assigned tier. As an example, if multiple risks have been identified over a wide variety of systems and require remediation, impacted team members can leverage the assigned critical system tiers to make a decision on how to prioritize remediation activities. This methodology will also provide GitLab with a mechanism to easily identify those systems which are most critical across the organization so that we can proactively protect and secure these systems.

### Maintaining Critical System Tiers

A critical system assessment is performed on an annual cadence in alignment with the [StORM annual risk assessment process](/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html) to validate existing systems in GitLab’s environment and make adjustments to assigned tiers accordingly. A system's assigned tier can be found in the [tech_stack.yml file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml).

### Future Iterations

This methodology is a baseline standard and future iterations are currently being considered to provide better granularity, if needed. As an example, if there are multiple Tier 1 systems that require some type of action, team memnbers should have a mechanism to prioritize work among these systems. Some future iterations which are currently being considered include:

* Utilizing a system's related tier and combining it with GitLab's [Data Classification Standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) to measure an overall system risk. 
* Utilizing a system's related tier to determine the proper system ownership to provide opportunities for centralizing administrative processes and procedures to ensure consistency in how a system is maintained and who is responsible for recovery activities against a standardized procedure

## Exceptions

Systems that are exempt from this methodology include any system which carries a data classification of Green. All remaining systems which store or process YELLOW, ORANGE, or RED data are required to have a critical system tier assigned. Data classification will be validated to corroborate that the data stored or processed by the system is truly green data, per the [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html#green).

## References

* [Business Continuity Plan (Business Impact Analysis Section)](/handbook/business-technology/gitlab-business-continuity-plan/#business-impact-analysis)
* [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html)
